CREATE TABLE legal_entities (
  lei VARCHAR(50),
  legal_name  VARCHAR(500),
  country  VARCHAR(50),
  status  VARCHAR(20)
);