package com.markit.gleif.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by jc on 11/02/2017.
 */
public class LegalAddress {

    private String country;

    @XmlElement(name = "Country", namespace="http://www.leiroc.org/data/schema/leidata/2014")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LegalAddress{");
        sb.append("country='").append(country).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
