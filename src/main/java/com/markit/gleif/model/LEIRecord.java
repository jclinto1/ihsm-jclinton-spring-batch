package com.markit.gleif.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LEIRecord", namespace="http://www.leiroc.org/data/schema/leidata/2014")
public class LEIRecord {

	private String lei;
	private Entity entity;

	@XmlElement(name = "LEI", namespace="http://www.leiroc.org/data/schema/leidata/2014")
	public String getLei() {
		return lei;
	}

	public void setLei(String lei) {
		this.lei = lei;
	}

	@XmlElement(name = "Entity", namespace="http://www.leiroc.org/data/schema/leidata/2014")
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public String getLegalName() {
		return getEntity().getLegalName();
	}

	public String getCountry() {
		return getEntity().getLegalAddress().getCountry();
	}

	public String getEntityStatus() {
		return getEntity().getEntityStatus();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("LEIRecord{");
		sb.append("lei='").append(lei).append('\'');
		sb.append(", entity=").append(entity);
		sb.append('}');
		return sb.toString();
	}
}
