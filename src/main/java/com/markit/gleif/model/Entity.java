package com.markit.gleif.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by jc on 11/02/2017.
 */
public class Entity {

    private String legalName;
    private LegalAddress legalAddress;
    private String entityStatus;

    @XmlElement(name = "LegalName", namespace="http://www.leiroc.org/data/schema/leidata/2014")
    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    @XmlElement(name = "LegalAddress", namespace="http://www.leiroc.org/data/schema/leidata/2014")
    public LegalAddress getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(LegalAddress legalAddress) {
        this.legalAddress = legalAddress;
    }

    @XmlElement(name = "EntityStatus", namespace="http://www.leiroc.org/data/schema/leidata/2014")
    public String getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(String entityStatus) {
        this.entityStatus = entityStatus;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Entity{");
        sb.append("legalName='").append(legalName).append('\'');
        sb.append(", legalAddress=").append(legalAddress);
        sb.append(", entityStatus='").append(entityStatus).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
