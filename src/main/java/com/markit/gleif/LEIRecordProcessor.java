package com.markit.gleif;

import com.markit.gleif.model.LEIRecord;
import org.springframework.batch.item.ItemProcessor;

public class LEIRecordProcessor implements ItemProcessor<LEIRecord, LEIRecord>{

	@Override
	public LEIRecord process(LEIRecord record) throws Exception {
		//System.out.println("Processing: " + record);
		return record;
	}

}
