package com.markit.gleif;

import com.markit.gleif.model.LEIRecord;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LEIRecordPreparedStatementSetter implements ItemPreparedStatementSetter<LEIRecord> {

	public void setValues(LEIRecord result, PreparedStatement ps) throws SQLException {
		ps.setString(1, result.getLei());
		ps.setString(2, result.getLegalName());
		ps.setString(3, result.getCountry());
		ps.setString(4, result.getEntityStatus());
	}

}